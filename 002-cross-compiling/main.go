package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Sistema operativo %v \n arquitectura %v \n", runtime.GOOS, runtime.GOARCH)
}
